package com.techdiffuse.loop;

class ForDemo {


    void doFor1() {
        // general form of the for statement
//        for (initialization; termination; increment) {
//            statement(s);
//        }

        for (int i = 1; i < 11; i++) {
            System.out.println("Count is: " + i);
        }
    }

    void doForInfinite() {
        int i = 0;
        for (;;) {
            System.out.println("Count is: " + i);
            i++;
        }
    }
}




