package com.techdiffuse.file_read_write;

import java.io.FileNotFoundException;

/**
 * Rotate a given matrix
 */
public class MatrixRotater {

    public void create(String path1, String[][] matrix) throws FileNotFoundException {

        String[][] gedrehteMatrix = new String[matrix.length][matrix[0].length];

        int downCount = matrix[0].length - 1;

        // rotate matrix
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                gedrehteMatrix[j][downCount] = matrix[i][j];
            }
            downCount--;
        }

        new MatrixCreator().create(path1, gedrehteMatrix);
    }
}
