package com.techdiffuse.file_read_write;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Read file 1 and write into file 2
 */
public class ReaderWriter {

    public void create(String path1, String path2) throws FileNotFoundException {

        Scanner sc1 = new Scanner(new File(path1));
        Formatter f = new Formatter(new File(path2));

        while (sc1.hasNext()) {
            f.format(sc1.next() + "   ");
        }
        sc1.close();
        f.close();
    }
}
