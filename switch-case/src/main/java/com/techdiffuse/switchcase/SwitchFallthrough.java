package com.techdiffuse.switchcase;

class SwitchFallthrough {
    String getTypeOfDay(int day) {

        String dayType;
        switch (day) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                dayType = "Weekday";
                break;
            case 6:
            case 7:
                dayType = "Day at weekend";
                break;

            default:
                dayType = "Invalid day";
        }

        return dayType;
    }
}

