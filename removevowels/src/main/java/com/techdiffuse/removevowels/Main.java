package com.techdiffuse.removevowels;

public class Main {

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Remove Vowels from a String");

        String toRemoveVowels = "In this string we want to remove all vowels. Vowels are a,e,i,o and u, as well as A,E,I,O and U.";
        System.out.println("toRemoveVowels = " + toRemoveVowels);


        String withoutVowels1 = removeVowels1(toRemoveVowels);
        System.out.println("withoutVowels1 = " + withoutVowels1);

        String withoutVowels2 = removeVowels2(toRemoveVowels);
        System.out.println("withoutVowels2 = " + withoutVowels2);

        String withoutVowels3 = removeVowels3(toRemoveVowels);
        System.out.println("withoutVowels3 = " + withoutVowels3);

        String withoutVowels4 = removeVowels4(toRemoveVowels);
        System.out.println("withoutVowels4 = " + withoutVowels4);
    }


    static String removeVowels1(String str) {
        return str.replaceAll("(a|e|i|o|u|A|E|I|O|U)", "");
    }


    static String removeVowels2(String str) {
        return str.replaceAll("[aeiouAEIOU]", "");
    }

    static String removeVowels3(String str) {
        String result = "";

        for (char c : str.toCharArray()) {
            if (!isVowel1(c)) {
                result = result + c;
            }
        }
        return result;
    }

    static String removeVowels4(String str) {
        str = str.replace("a", "");
        str = str.replace("e", "");
        str = str.replace("i", "");
        str = str.replace("o", "");
        str = str.replace("u", "");
        str = str.replace("A", "");
        str = str.replace("E", "");
        str = str.replace("I", "");
        str = str.replace("O", "");
        str = str.replace("U", "");
        return str;
    }

    static boolean isVowel3(char ch) {
        switch (ch) {
            case 'a':
            case 'A':
            case 'e':
            case 'E':
            case 'i':
            case 'I':
            case 'o':
            case 'O':
            case 'u':
            case 'U':
                return true;
            default:
                return false;
        }
    }


    static boolean isVowel2(char ch) {
        if (ch == 'a' || ch == 'A' ||
                ch == 'e' || ch == 'E' ||
                ch == 'i' || ch == 'I' ||
                ch == 'o' || ch == 'O' ||
                ch == 'u' || ch == 'U') {
            return true;
        }
        return false;
    }


    static boolean isVowel1(char ch) {
        if (ch == 'a')
            return true;
        if (ch == 'A')
            return true;
        if (ch == 'e')
            return true;
        if (ch == 'E')
            return true;
        if (ch == 'i')
            return true;
        if (ch == 'I')
            return true;
        if (ch == 'o')
            return true;
        if (ch == 'O')
            return true;
        if (ch == 'u')
            return true;
        if (ch == 'U')
            return true;
        return false;
    }
}

