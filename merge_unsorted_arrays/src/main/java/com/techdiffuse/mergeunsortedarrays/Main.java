package com.techdiffuse.mergeunsortedarrays;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Merge unsorted Arrays and sort them");

        int aLen = 90000333;
        int bLen = 91233433;

        int a[] = new int[aLen];
        for (int i = 0; i < aLen; i++) {
            a[i] = (int) (Math.random() * aLen);
        }

        int b[] = new int[bLen];
        for (int i = 0; i < bLen; i++) {
            b[i] = (int) (Math.random() * bLen);
        }

//        int a[] = {10, 5, 15, 7, 6, 4};
//        int b[] = {20, 3, 2, 12};
//
//        System.out.println("\na");
//        for (int i : a) {
//            System.out.print(i + " ");
//        }
//
//        System.out.println("\n\nb");
//        for (int i : b) {
//            System.out.print(i + " ");
//        }

//        int a[] = {1,1,1,1,1,1,1,1,1};
//        int b[] = {2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2};

        // measure time start
        Instant start1 = Instant.now();

        // aLen = 200003336, bLen = 212334333, timeElapsed1 -> 66,89
        // aLen = 20000333, bLen = 21233433, timeElapsed1 -> 4,7
        // aLen = 50000333, bLen = 51233433, timeElapsed1 -> 11
        // aLen = 90000333, bLen = 91233433, timeElapsed1 -> 23
        int[] sortedMerge = sortedMerge1(a, b);

        // aLen = 200003336, bLen = 212334333, timeElapsed1 -> heap space error, sogar mit 8192
        // aLen = 20000333, bLen = 21233433, timeElapsed1 -> 4,7
        // aLen = 50000333, bLen = 51233433, timeElapsed1 -> 13,7
        // aLen = 90000333, bLen = 91233433, timeElapsed1 -> 22,2
//        int[] sortedMerge = sortedMerge2(a, b);
//
        // measure time end
        Instant finish1 = Instant.now();
        final long timeElapsed1 = Duration.between(start1, finish1).toNanos();
        System.out.println("\n\ntimeElapsed1 = " + BigDecimal.valueOf(timeElapsed1 / 1_000_000_000.0));
//
//        System.out.println("\nafter");
//        System.out.println("\na");
//        for (int i : a) {
//            System.out.print(i + " ");
//        }
//
//        System.out.println("\n\nb");
//        for (int i : b) {
//            System.out.print(i + " ");
//        }
//
//        System.out.println("\n\nsortedMerge");
//        for (int i : sortedMerge) {
//            System.out.print(i + " ");
//        }
//        System.out.println();
    }

    /**
     * @param a
     * @param b
     * @return
     */
    static int[] sortedMerge2(int a[], int b[]) {

        // Sorting a[] and b[]
        Arrays.sort(a);
        Arrays.sort(b);

        int mergedArray[] = new int[a.length + b.length];

        // Merge two sorted arrays into mergedArray[]
        int i = 0, j = 0, k = 0;

        // raise i as long as we are not at the end of a
        // raise j as long as we are not at the end of b
        while (i < a.length && j < b.length) {
            // if element in a is smaller or equal than element in b
//            System.out.print("a.len: " + a.length + "   a[" + i + "]:" + a[i] + "          b.len: " + b.length + "   b[" + j + "]:" + b[j] + "          k: " + k);
            if (a[i] <= b[j]) {
                // put the smaller element in the result array
                mergedArray[k] = a[i];
                i++;
                // if element in a is bigger than element in b
            } else {
                // put the smaller element in the result array
                mergedArray[k] = b[j];
                j++;
            }
            k++;

//            System.out.println("\nloop mergedArray");
//            for (int z : mergedArray) {
//                System.out.print(z + " ");
//            }
//            System.out.println("\n");
        }

        // at this point, either i has reached the end of a or j has reached the end of b
//        System.out.println("\n\nintermediate mergedArray");
//        for (int z : mergedArray) {
//            System.out.print(z + " ");
//        }

        // merge remaining elements of a[] (if any)
        while (i < a.length) {
//            mergedArray[k] = a[i];
//            i++;
//            k++;
            mergedArray[k++] = a[i++];
        }
        // merge remaining elements of b[] (if any)
        while (j < b.length) {
//            mergedArray[k] = b[j];
//            j++;
//            k++;
            mergedArray[k++] = b[j++];
        }

        return mergedArray;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    static int[] sortedMerge1(int a[], int b[]) {
        // create array with length of a + length of b
        int mergedArray[] = new int[a.length + b.length];

        for (int i = 0; i < a.length; i++) {
            mergedArray[i] = a[i];
        }
        for (int i = 0; i < b.length; i++) {
            mergedArray[i + a.length] = b[i];
        }

        Arrays.sort(mergedArray);
        return mergedArray;
    }


}
