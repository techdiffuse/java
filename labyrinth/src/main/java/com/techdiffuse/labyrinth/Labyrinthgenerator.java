package com.techdiffuse.labyrinth;


/**
 * Diese Klasse enth�lt eine Reihe von Beispiellabyrinthen und eine Funktion, um aus der
 * textuellen Darstellung eines Laybrinths ein zweidimensionales char-Feld zu erzeugen.
 * Die Benutzung ist z.B. wie folgt:
 *
 * char[][] labyrinth = Labyrinthgenerator.erzeugeLabyrinth(Labyrinthgenerator.LABYRINTHE[0])
 *
 * Es k�nnen auch eigene Labyrinthe damit erzeugt werden. Wichtig ist nur, dass jede "Zeile" des Labyrinths mit einem Zeilenumbruch abgeschlossen wird (\n).
 */
public class Labyrinthgenerator {

	public final static String[] LABYRINTHE = {

		// 0
		"█████████\n" +
		"█   █   █\n" +
		"███ █ █ █\n" +
		"Z     █ █\n" +
		"███████ █\n" +
		"█     █ █\n" +
		"█ ███ █ █\n" +
		"█   █   █\n" +
		"███S█████\n",

		// 1
		"███████████████\n" +
		"█           █ █\n" +
		"█ ███████ █ █ █\n" +
		"█     █   █   █\n" +
		"█████ █████████\n" +
		"S   █         █\n" +
		"█ █ █ ███████ █\n" +
		"█ █ █   █   █ █\n" +
		"█ █ █ █ █ ███ █\n" +
		"█ █ █ █ █   █ █\n" +
		"███ █ █ ███ █ █\n" +
		"█   █ █   █ █ █\n" +
		"█ █████ █ █ █ █\n" +
		"█       █   █ █\n" +
		"█████████████Z█\n",

		// 2
		"█████████████████████████████\n" +
		"S    █ █     █         █    █\n" +
		"████ █ █  ████ ██████ ██ ██ █\n" +
		"████ █   █   █ █    █ █  ██ █\n" +
		"█    █ ████  █ █    █ █   █ █\n" +
		"█ ████ █     █ █    █ ███ █ █\n" +
		"█ █    ████  █ ███  █ █   █ █\n" +
		"█ █ ███      █ █    █   ███ █\n" +
		"█       ████   █      █   █ █\n" +
		"███████████████████████████ █\n" +
		"Z █   █   █   █   █   █   █ █\n" +
		"█   █   █   █   █   █   █   █\n" +
		"█████████████████████████████\n",

		// 3
		"█████████████████████████████████\n" +
		"█         █ █    ████     ██    █\n" +
		"█ ███   █   █    █    ███    ████\n" +
		"█   █   █ ███    █    █  █ █ █  █\n" +
		"███ █   █        █ █████ █ █ █  █\n" +
		"█   █   █ ████████     █        █\n" +
		"█ ███   █      █ █ █████ ████   █\n" +
		"█ █ █   ██████ █ █ █ █ █ █  █   █\n" +
		"█ █ █   █   ██ █ █ █ █ █ █  █████\n" +
		"█    █  █ █S     █ █ █          █\n" +
		"█ █ ██████████████ █ █  █████ █ █\n" +
		"█ █ ████████████ █ █ █     ██ █ █\n" +
		"█                  █ █ ███    █ █\n" +
		"█  █████████ ███████ █ █   ████ █\n" +
		"█  █       █ █ █       █     █  █\n" +
		"█  █ █  █ ████ █ █████ ██████   █\n" +
		"████ █  █        █ █ █      █   █\n" +
		"█    ██████      █   █      █   █\n" +
		"█  █             █          █   █\n" +
		"██████████Z██████████████████████\n",

		// 4
		"█████████████████████████████████\n" +
		"Z    █               ██         █\n" +
		"████ ██ █ ██████████ ██ ███████ █\n" +
		"█  █ █  ██         █ ██       █ █\n" +
		"█  █ ██ ██ ███████ █ ██ ███████ █\n" +
		"█  █ █  █  █     █ █ ██       █ █\n" +
		"█  █ ██ ██ █ ███ █ █ ██ ███████ █\n" +
		"█  █ █  █  █ █S  █ █ ██       █ █\n" +
		"█  █ ██ ██ █ █████ █ ██ ███████ █\n" +
		"█  █ █  █  █       █ ██       █ █\n" +
		"█  █ ██ ██ █████████ ██ ███████ █\n" +
		"█  █ █  █            ██       █ █\n" +
		"█  █ ██ ███████████████ ███████ █\n" +
		"█  █ █                        █ █\n" +
		"█  █ ██████████████████████████ █\n" +
		"█                               █\n" +
		"█████████████████████████████████\n",

		// 5
		"████\n" +
		"S  Z\n" +
		"████\n",

		// 6
		"██S█████████████████████\n" +
		"█  █      █            █\n" +
		"█ █  ████ █ ██████████ █\n" +
		"█   █   █ █         █  █\n" +
		"█ ████  █ █ █████   █ ██\n" +
		"█ █     █ █     █ ███  █\n" +
		"█ █ █████ █ █████   ██ █\n" +
		"█ █ █   ███ █       █  █\n" +
		"█     █     █       █ ██\n" +
		"█████████████████████Z██\n",

		// 7
		"██Z█████████████████████\n" +
		"██ █      █            █\n" +
		"██ █ ████ █ ████████████\n" +
		"█       █ █         █  S\n" +
		"██████  █ █ ██████  █ ██\n" +
		"█ █     █ █      █ ██  █\n" +
		"█ █ █████ █ ██████  ██ █\n" +
		"█ █ █   ███ █    █  █  █\n" +
		"█     █     █    █    ██\n" +
		"████████████████████████\n"
	};

	/**
	 * Erzeugt aus der textuellen Darstellung eines Labyrinths seine Repräsentation aks zweidimensionales char-Feld.
	 * Die erste Dimension entsprich dabei den "Zeilen" des Labyrinths, die zweite den "Spalten".
	 *
	 * @param text ein Labyrinth als String
	 * @return ein zweidimensionales char-Feld des Labyrinths
	 */
	public static char[][] erzeugeLabyrinth(String text) {
		String[] zeilen = text.split("\n"); // zerhackt die Zeichenkette an Newlines
		char[][] labyrinth = new char[zeilen.length][];

		for (int i = 0; i < zeilen.length; i++) {
			labyrinth[i] = zeilen[i].toCharArray();
		}

		return labyrinth;
	}
}
