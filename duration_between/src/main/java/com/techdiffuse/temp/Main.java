package com.techdiffuse.temp;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;


public class Main {

    public static void main(String args[]) {
        Main main = new Main();
        main.differenceWithDurationBetween();
    }


    /**
     * LocalDateTime, Duration
     * A java.time.Duration example to find out
     * the seconds between two LocalDateTime
     */
    public void differenceWithDurationBetween() {
        LocalDateTime oldDate = LocalDateTime.of(2018, Month.AUGUST, 31, 10, 20, 55);
        LocalDateTime newDate = LocalDateTime.of(2018, Month.NOVEMBER, 9, 10, 21, 56);

        System.out.println("Duration.between");
        System.out.println(oldDate);
        System.out.println("and");
        System.out.println(newDate);
        System.out.println("=");

        // count seconds between dates
        Duration duration = Duration.between(oldDate, newDate);
        System.out.println(duration.getSeconds() + " seconds");
    }

}










