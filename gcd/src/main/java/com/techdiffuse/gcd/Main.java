package com.techdiffuse.gcd;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println();
        System.out.println("Find GCD");

        int a = 98;
        int b = 56;
        a = 9;
        b = 81;
        // Test
//        a = (int) (Math.random() * 1000) + 1;
//        b = (int) (Math.random() * 1000) + 1;


        int arr1 = (int) (Math.random() * 100) + 1;
        int arr2 = (int) (Math.random() * 100) + 1;
        int arr3 = (int) (Math.random() * 100) + 1;
        int arr4 = (int) (Math.random() * 100) + 1;
        int arr5 = (int) (Math.random() * 100) + 1;
        int gcd[] = {81, 63, 27, 9, 3};
//        int gcd[] = {arr1, arr2, arr3};

        System.out.println("gcdFor1          Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdFor1(a, b));
        System.out.println("gcdFor2          Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdFor2(a, b));
        System.out.println("gcdWhile         Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdWhile(a, b));
        System.out.println("gcdRecursive1    Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdRecursive1(a, b));
        System.out.println("gcdRecursive1_1  Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdRecursive1_1(a, b));
        System.out.println("gcdRecursive1_2  Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdRecursive1_2(a, b));
        System.out.println("gcdRecursive2    Greatest Common Divisor of [" + a + ", " + b + "] = " + gcdRecursive2(a, b));
        System.out.println("gcdArray         Greatest Common Divisor of " + Arrays.toString(gcd) + " = " + gcdArray(gcd));
    }

    static int gcdFor1(int a, int b) {
        int gcd = 1;

        for (int i = 1; i <= a && i <= b; i++) {
//            System.out.println("i = " + i);
            if (a % i == 0 && b % i == 0) {
                gcd = i;
//                System.out.println("gcd = " + gcd);
            }
        }
        return gcd;
    }

    static int gcdFor2(int a, int b) {

//        int start = 0;
//        if (a > b)
//            start = b;
//        else
//            start = a;

        int start = a > b ? b : a;
//        System.out.println("start = " + start);

        for (int i = start; i > 0; i--) {
            if (a % i == 0 && b % i == 0) {
//                System.out.println("i = " + i);
                return i;
            }
        }
        return -1;
    }

    static int gcdWhile(int a, int b) {
//        System.out.println("a = " + a +"   b = " + b);
        while (a != b) {
            if (a > b)
                a = a - b;
            else
                b = b - a;
//            System.out.println("a = " + a +"   b = " + b);
        }
        return b;
    }

    /**
     * classical Euclidean algorithm
     */
    static int gcdRecursive1(int a, int b) {
        if (b == 0)
            return a;
        if (a > b) {
//            System.out.println("a = " + a + "   b = " + b + "   " + a + " - " + b + " = " + (a - b));
            return gcdRecursive1(b, a - b);
        } else {
//            System.out.println("a = " + a + "   b = " + b + "   " + b + " - " + a + " = " + (b - a));
            return gcdRecursive1(b, b - a);
        }
    }

    /**
     * classical Euclidean algorithm
     */
    static int gcdRecursive1_1(int a, int b) {
        if (a == b)
            return a;
        if (a > b) {
//            System.out.println("a = " + a + "   b = " + b + "   " + a + " - " + b + " = " + (a - b));
//            return gcdRecursive1_1(b, a - b);
            return gcdRecursive1_1(a - b, b);
        } else {
//            System.out.println("a = " + a + "   b = " + b + "   " + b + " - " + a + " = " + (b - a));
//            return gcdRecursive1_1(a, b - a);
            return gcdRecursive1_1(b - a, a);
        }
    }

    /**
     * classical Euclidean algorithm
     */
    static int gcdRecursive1_2(int a, int b) {
        if (a == b)
            return a;
        if (a > b) {
//            System.out.println("a = " + a + "   b = " + b + "   " + a + " - " + b + " = " + (a - b));
//            return gcdRecursive1_2(b, a - b);
            return gcdRecursive1_2(a - b, b);
        } else {
//            System.out.println("a = " + a + "   b = " + b + "   " + b + " - " + a + " = " + (b - a));
//            return gcdRecursive1_2(a, b - a);
            return gcdRecursive1_2(b - a, a);
        }
    }

    /**
     * modern Euclidean algorithm
     */
    static int gcdRecursive2(int a, int b) {
        if (b == 0)
            return a;
//        System.out.println("a = " + a + "   b = " + b + "   " + a + " % " + b + " = " + (a % b));
        return gcdRecursive2(b, a % b);
    }


    static int gcdArray(int[] a) {
        int result = a[0];
        for (int i = 1; i < a.length; i++) {
            System.out.print("a[" + i + "] = " + a[i] + "   result = " + result + "     gcd(" + a[i] + "," + result + ") = ");
            result = gcdRecursive1(a[i], result);
            System.out.println(result);
        }
        return result;
    }
}

