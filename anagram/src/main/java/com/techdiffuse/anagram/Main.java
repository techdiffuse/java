package com.techdiffuse.anagram;

public class Main {

    public static void main(String[] args) {

        String s1 = "Mildred Smedley";
        String s2 = "slid remedy meld";

        System.out.println();

        System.out.println("s1 = " + s1);
        System.out.println("s2 = " + s2);

//        String s1 = "reliefpfeiler";
//        String s2 = "reliefpfeiler";

        System.out.println();

        s1 = s1.replace(" ", "").trim().toLowerCase();
        s2 = s2.replace(" ", "").trim().toLowerCase();

        System.out.println("\nisAnagram1: " + new Anagram().isAnagram1(s1, s2));
        System.out.println("\nisAnagram2: " + new Anagram().isAnagram2(s1, s2));
    }
}
