package com.techdiffuse.labyrinth;

/**
 * Run on a console (not in an IDE) to get the best visual representation
 */
public class Main {

    static int schritte;

    public static void main(String[] args) throws InterruptedException {

        char[][] Labyrinth = Labyrinthgenerator.erzeugeLabyrinth(Labyrinthgenerator.LABYRINTHE[4]);

        int i1 = 0, j1 = 0, i2 = 0, j2 = 0;

        for (int i = 0; i < Labyrinth.length; i++) {
            for (int j = 0; j < Labyrinth[i].length; j++) {
                if (Labyrinth[i][j] == 'S') {
                    i1 = i;
                    j1 = j;
                }
                if (Labyrinth[i][j] == 'Z') {
                    i2 = i;
                    j2 = j;
                }
            }
        }
        System.out.println("Start = " + i1 + "_" + j1);
        System.out.println("Ziel = " + i2 + "_" + j2);

        boolean gefunden = durchlaufeLab(Labyrinth, i1, j1, 0);

        System.out.println("Ziel gefunden = " + gefunden);
        System.out.println("Schritte = " + (schritte + 1));
    }

    /**
     * Lab durchlaufen
     *
     * @param Lab
     * @param i1
     * @param j1
     * @param count
     * @return
     */
    static boolean durchlaufeLab(char[][] Lab, int i1, int j1, int count) throws InterruptedException {
        System.out.print("\033[H\033[2J"); // clear the console
        System.out.flush();

        for (char[] hoehe : Lab) {
            for (char c : hoehe) {
                System.out.print(c);
            }
            System.out.print("\n");
        }
        System.out.println("\n");

        Thread.currentThread().sleep(70);

        // unten, rechts, oben, links
        final int[] deltaX = {1, 0, -1, 0};
        final int[] deltaY = {0, 1, 0, -1};

        // Schleife aller möglichen Zuege
        for (int i = 0; i < deltaX.length; i++) {

            // neue Position
            int newi1 = i1 + deltaX[i];
            int newj1 = j1 + deltaY[i];

            // Prüfen ob der neue Punkt im Array liegt
            boolean imFeld = false;

            if (newi1 < Lab.length && newj1 < Lab[0].length)
                imFeld = true;
            else
                continue;

            // Ziel gefunden
            if (imFeld && Lab[newi1][newj1] == 'Z') {
                schritte = count;
                return true;
            }

            // Nächste Position noch leer -> Nächster Zug
            if (Lab[newi1][newj1] == ' ') {

                // Markieren bzw. belegen
                Lab[newi1][newj1] = '•';

                if (durchlaufeLab(Lab, newi1, newj1, count + 1)) {
                    // Weg gefunden, Funktionsende!
                    return true;
                }

                System.out.println("Sackgasse!");
                Lab[newi1][newj1] = '!';
            }
        }
        return false;
    }
}
