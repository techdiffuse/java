package com.techdiffuse.swap_row;

public class Matrix {

    /**
     * standard for loop
     *
     * @param matrix
     * @param row1
     * @param row2
     */
    void swapRow1(int[][] matrix, int row1, int row2) {
        for (int i = 0; i < matrix[0].length; i++) {
            int temp = matrix[row1][i];
            matrix[row1][i] = matrix[row2][i];
            matrix[row2][i] = temp;
        }
        printArray(matrix);
    }

    /**
     * print matrix
     *
     * @param matrix
     */
    void printArray(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%4d  ", matrix[i][j]);
            }
            System.out.println();
        }
    }


}
