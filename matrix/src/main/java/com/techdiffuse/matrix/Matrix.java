package com.techdiffuse.matrix;

public class Matrix {

    public static void reverse(int zahl[], char zeichen[]) {

        char charFeld2[] = new char[zeichen.length];
        int intFeld2[] = new int[zahl.length];

        int downCount = zahl.length - 1;
        for (int i = 0; i < zahl.length; i++) {
            intFeld2[downCount] = zahl[i];
            charFeld2[downCount] = zeichen[i];
            downCount--;
        }

        System.out.print("Umgedrehte Zeichen:   ");
        for (char c : charFeld2)
            System.out.print(c + " ");
        System.out.println();

        System.out.print("Umgedrehte Zahlen:    ");
        for (int i : intFeld2) {
            System.out.print(i + " ");
        }
    }

    public void rotate2() {
        // 3 Zeilen, 3 Spalten
        // Aeussere geschweifte Klammern == Komplettes Array
        // Erste innere == Erste Zeile
        String[][] matrix = {{"0_0", "0_1", "0_2"}, {"1_0", "1_1", "1_2"}, {"2_0", "2_1", "2_2"}};
        String[][] gedreht = new String[matrix.length][matrix[0].length];

        System.out.println("Ausgabe des Original Array : ");
        for (String[] zeile : matrix) {
            for (String spalte : zeile) {
                System.out.print(spalte + "   ");
            }
            System.out.println();
        }

        int downCount = matrix[0].length - 1;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                gedreht[j][downCount] = matrix[i][j];
            }
            downCount--;
        }

        // Ausgabe mit for-each
        System.out.println("\nTest ");
        for (String[] zeile : gedreht) {
            for (String spalte : zeile) {
                System.out.print(spalte + "   ");
            }
            System.out.println();
        }

    }

    public void rotate1() {
        // 3 Zeilen, 1 Spalte
        // String[][] matrix = { { "0,0" }, { "0,1" }, { "0,2" } };

        // 9 Zeilen, 1 Spalte
        // String[][] matrix = { { "0,0" }, { "0,1" }, { "0,2" }, { "1,0" }, { "1,1" }, { "1,2" }, { "2,0" }, { "2,1" }, { "2,2" } };

        // 3 Zeilen, 3 Spalten
        // Aeussere geschweifte Klammern == Komplettes Array
        // Erste innere == Erste Zeile (Laenge ist die Anzahl der Werte durch Kommas getrennt

        String[][] matrix = {{"0_0", "0_1", "0_2"}, {"1_0", "1_1", "1_2"}, {"2_0", "2_1", "2_2"}};
        String[][] temp = new String[matrix.length][matrix[0].length];

        System.out.println("\nOriginal Array: ");
        for (String[] row : matrix) {
            for (String element : row) {
                System.out.print(element + "   ");
            }
            System.out.println();
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                temp[j][i] = matrix[i][j];
            }
        }

        System.out.println("\nRotated Array: ");
        for (String[] zeile : temp) {
            for (String spalte : zeile) {
                System.out.print(spalte + "   ");
            }
            System.out.println();
        }

    }


    void create5(int[][] matrix) {

        // Testausgabe
        for (int[] width : matrix) {
            for (int element : width) {
                System.out.printf("%4d ", element);
            }
            System.out.println();
        }

        // create an array that has the length of on array
        int[] zeilensumme = new int[matrix.length];

        // Addition der Werte in einer Zeile
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                zeilensumme[i] += matrix[i][j];
            }
        }

        int i = 1;
        for (int zahl : zeilensumme) {
            System.out.println("Summe in " + i++ + " ter Zeile = " + zahl);
        }
    }

    /**
     * print passed matrix
     *
     * @param matrix
     */
    void create4(int[][] matrix) {

        // print matrix
        for (int[] width : matrix) {
            for (int element : width) {
                System.out.printf("%4d ", element);
            }
            System.out.println();
        }

        System.out.println("matrix.length = " + matrix.length);
        System.out.println("matrix[0] = " + matrix[0]);
        System.out.println();

        int summe = 0;
        for (int i = 0; i < matrix.length; i++) {
            try {
                summe = summe + matrix[i][i];
                System.out.println("matrix[" + i + "][" + (i) + "] = " + matrix[i][i]);
            } catch (Exception e) {
                System.out.println("Error");
            }
        }
        System.out.println("Die Summe beträgt = " + summe);
    }

    /**
     * init array with one dimension and emulate 2 dimensions
     *
     * @param rows
     * @param cols
     */
    void create3(int rows, int cols) {
        int iab[][] = new int[rows * cols][];

        for (int i = 0; i < iab.length; i++) {
            if (i % cols == 0)
                System.out.println("");
            System.out.printf("%4d ", i);
        }
    }

    /**
     * Array
     *
     * @param rows
     * @param cols
     */
    void create2(int rows, int cols) {
        int iab[][] = new int[rows][cols];

        for (int i = 0; i < iab.length; i++) {
            int temp = (i + 1) * 100;
            for (int j = 0; j < iab[i].length; j++) {
                iab[i][j] = temp + (j + 1);
            }
        }

        int a = 0;
        int b = 1;
        int c = 2;

        System.out.println("iab[" + a + "][" + a + "] = " + iab[a][a]);
        System.out.println("iab[" + c + "][" + b + "] = " + iab[c][b]);

        for (int[] zeile : iab) {
            for (int spalte : zeile) {
                System.out.printf("%5d ", spalte);
            }
            System.out.println();
        }
    }

    /**
     * Array
     *
     * @param rows
     * @param cols
     */
    void create1(int rows, int cols) {

        int[][] matrix = new int[rows][cols];

        // fill matrix and print
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }


}
